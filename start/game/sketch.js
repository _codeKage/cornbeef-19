var gameManager;
let bg;
function setup(){
    bg = loadImage('assets/bg.jpg');
    createCanvas(500,500);
    gameManager = new GameManager();
}

function draw() {
    background(bg);   
    gameManager.render();
}

