class Enemy {

    constructor(location, height, width) {
        this.location = location;
        this.height = height;
        this.width = width;
        this.img = loadImage('assets/enemy.png');
    }

    update(playerPos) {
        const vel = playerPos.copy().sub(this.location);
        vel.normalize();
        vel.mult(5);
        this.location.add(vel);
    }

    display() {
        push();
        imageMode(CENTER);
        image(this.img, this.location.x, this.location.y, this.width, this.height);
        pop();
    }

    onEdges(){
        return this.location.y > (height + this.height);
    }

    collided(side,loc){
        return (side + this.height) > this.location.dist(loc);
    }

}