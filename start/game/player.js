class Player {
    constructor(location, height, width) {
        this.location = location;
        this.height = height;
        this.width = width;
        this.img = loadImage('assets/asd.png');
    }

    display() {
        push();
        imageMode(CENTER);
        image(this.img, this.location.x, this.location.y, 100, 100);
        pop();
    }

    update() {
      this.location.x = mouseX;
      this.location.y = mouseY;
    }

}