class GameManager {

    constructor() {
        this.player = new Player(createVector(mouseX, mouseY), 5, 20);
        this.enemies = [];
        this.score = 0;
        console.log(mouseX, mouseY);
    }

    render() {
        this.displayPlayer();
        this.generateEnemy();
        this.displayEnemies();
    }

    displayPlayer(){
        this.player.update();
        this.player.display();
        // this.displayScore();
    }

    generateEnemy() {
        if(frameCount % 120 === 0){
            let enemyCount = Math.round(random(1,4));

            for(let i = 0; i < enemyCount; i++){

            this.enemies.push(new Enemy(
                   createVector(random(0, 500),random(0, 500)),
                   50,
                   50
               ));
            }

          }

    }

    displayScore() {
        textSize(30);
        fill(color(0, 0, 0));
        text("Score: "+Math.round(this.score),width/2.7,50);
    }

    displayEnemies() {
        this.enemies.forEach((enemy, index) => {
            if (enemy.onEdges()) {
                this.enemies.splice(index, 1);
                this.score++;
            } else if (enemy.collided(this.player.height - 30, this.player.location)) {
                alert('Infected');
                this.score = 0;
                this.enemies.splice(index, 1);
                location.reload();
            } else {
                try {
                    this.enemies[index].update(this.player.location);
                    this.enemies[index].display();
                } catch(exception) {

                }

            }
        });
    }

}